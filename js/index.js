//THe main engine of collecting data and visualization with svg
var svg_width = 800;
var svg_height = 800;
var cellHeight = 55;
var cellWidth= 30;
var upperRectHeight = 10;
var upperRectWidth = cellWidth/3;
var xMargin = svg_width/2 - 10;
var yMargin = 10;
var lowerDomain = 2.8;
var upperDomain = 4.2;
var entireData;
var comboBox;
var svg;
var battGroup;

var updateSVG = function(packData){
	//var svg = d3.select("svg").append("svg");
	
	var linearScaler = d3.scaleLinear().domain([lowerDomain, upperDomain]).range([0,cellHeight-upperRectHeight]);
	var colorScaler = d3.scaleLinear().domain([lowerDomain, upperDomain]).range(["red", "green"]);
	
	//console.log(packData);

	var dataUpdate = battGroup.selectAll("rect").data(packData);

	dataUpdate.attr("x",function(d){return xMargin;})
		.attr("y",function(d,i){return (i+1)*cellHeight - linearScaler(d) +yMargin;})
		.attr("width", function(d){return cellWidth;})
		.attr("height", function(d,i){return linearScaler(d);})
		.attr("fill", function(d){return colorScaler(d)});

	var dataEnter = dataUpdate.enter();

	//data
	dataEnter.append("rect")
		.attr("x",function(d){return xMargin;})
		.attr("y",function(d,i){return (i+1)*cellHeight - linearScaler(d) +yMargin;})
		.attr("width", function(d){return cellWidth;})
		.attr("height", function(d,i){return linearScaler(d);})
		.attr("fill", function(d){return colorScaler(d)});

	dataUpdate.exit().remove();


	var textUpdate = battGroup.selectAll("text").data(packData);

	textUpdate.attr("x",xMargin + cellWidth + 15)
		.attr("y",function(d,i){ return cellHeight*i + yMargin + upperRectHeight + cellHeight/2;})
		.text(function(d,i){return "Cell " + (i+1) + " - Voltage: " + d;});

	var textEnter = textUpdate.enter();

	textEnter.append("text")
				.attr("x",xMargin + cellWidth + 15)
				.attr("y",function(d,i){ return cellHeight*i + yMargin + upperRectHeight + cellHeight/2;})
				.text(function(d,i){return "Cell " + (i+1) + " - Voltage: " + d;});
	textUpdate.exit().remove();

};

var comboOnChange = function(){
	//d3.select("g").remove();
	var selectValue = d3.select("select").property("value");
	getVoltageArray(entireData,selectValue, updateSVG);
};

var drawCombo = function(data) {
	comboBox = d3.select("select").attr("width","100%").on("change",comboOnChange);
	var options = comboBox.selectAll("option")
						.data(data)
						.enter()
							.append("option")
							.text(function(d){return (new Date(d)).toString();})
							.attr("value",function(d){return d;});
};

var draw = function(data){
	//This is basically where all the code will happen!
	entireData = data; //just for a global reference
	svg = d3.select("svg");
	svg.attr("width", svg_width + "px");
	svg.attr("height", svg_height + "px");
	battGroup = svg.append("g");
	//draw static elements (bounding box and the top)
	for(var x=0; x<14; x++){
		svg.append("rect")
		.attr("x",xMargin + upperRectWidth)
		.attr("y",cellHeight*x + yMargin)
		.attr("width",upperRectWidth)
		.attr("height",upperRectHeight)
		.attr("fill", "#86849b");

		svg.append("rect")
		.attr("x",xMargin)
		.attr("y",cellHeight*x + yMargin + upperRectHeight)
		.attr("width",cellWidth)
		.attr("height", cellHeight-upperRectHeight)
		.attr("fill", "none")
		.attr("stroke-width", 3)
		.attr("stroke", "#009900");
	}


	getVoltageArray(data,data[0]["Time"], updateSVG);
	getTimeArray(data, drawCombo);
};

var getVoltageArray = function(data,time,callback){
	var result;
	data.forEach(function(cell,index){
		if(cell["Time"] == time){
			var arry = [];
			for(var x=1; x<=14; x++){
				arry[x-1] = cell["Cell " + x];
			}
			result = arry;
		}
	});
	if(callback){
		callback(result);
	}
	return result;
};

var getTimeArray = function(data, callback){
	var result = [];
	var x=0;
	data.forEach(function(element){
		result[x++] = element["Time"];
	});

	if(callback){
		callback(result);
	}
	return result;
};

d3.csv("../data/B-D000009.csv",function(UnparsedData){
	return {
		Time: Date.parse(UnparsedData["Time"]),
		"Cell 1": +UnparsedData["Cell 1"],
		"Cell 2": +UnparsedData["Cell 2"],
		"Cell 3": +UnparsedData["Cell 3"],
		"Cell 4": +UnparsedData["Cell 4"],
		"Cell 5": +UnparsedData["Cell 5"],
		"Cell 6": +UnparsedData["Cell 6"],
		"Cell 7": +UnparsedData["Cell 7"],
		"Cell 8": +UnparsedData["Cell 8"],
		"Cell 9": +UnparsedData["Cell 9"],
		"Cell 10": +UnparsedData["Cell 10"],
		"Cell 11": +UnparsedData["Cell 11"],
		"Cell 12": +UnparsedData["Cell 12"],
		"Cell 13": +UnparsedData["Cell 13"],
		"Cell 14": +UnparsedData["Cell 14"],
		"Pack Voltage": +UnparsedData["Pack Voltage"]
	};
}, draw);
